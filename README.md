# docker-drupal

This version of image is for only development purpose

Create image
./build.sh

First run the image in your working directory.

```
  docker run -ti -v $(pwd):/drupal attek/drupal:9.1
```

Change settings in .env file, and run helpers/up

