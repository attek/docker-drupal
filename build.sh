docker pull php:cli 
docker run -ti -v $PWD/xml.php:/xml.php php:cli php /xml.php > /tmp/versions
IFS=" "
while read version fullVersion url md5
do
    echo "Build: $fullVersion, Tag: $version"
    docker build --build-arg="DRUPAL_VERSION=$fullVersion" -t attek/drupal:$version .
done < /tmp/versions